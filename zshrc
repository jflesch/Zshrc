cd $HOME

unset MAILCHECK

# My functions (don't forget to modify fpath before call compinit !!)
fpath=($HOME/.zsh/functions $fpath)


if [[ -r $HOME/.zen ]] ; then
	fpath=($fpath $HOME/.zen/zsh/scripts $HOME/.zen/zsh/zle)
	autoload -U zen
fi

# in order to have many completion function run comptinstall !

autoload -U zutil
autoload -U compinit
autoload -U complist

# Activation
compinit

local os host

# Set default umask to 027, can be override by host specific resource file
umask 022

# per host resource file
host=${$(hostname)//.*/}
if [[ -f "$HOME/.zsh/rc.host/${host}.zsh" ]] ; then
	 source "$HOME/.zsh/rc.host/${host}.zsh"
else
	source "$HOME/.zsh/rc.host/default.zsh"
fi

# per OS resource file
os=$(uname)
[[ -f "$HOME/.zsh/rc.os/${os}.zsh" ]] && source "$HOME/.zsh/rc.os/${os}.zsh"

# Global resource files
for file in $HOME/.zsh/rc/*.rc; do
	source $file
done

# Local file
[[ -f ~/.zsh/rc.local ]] && source ~/.zsh/rc.local

true

export NVM_DIR="$HOME/.nvm"
[[ -s "$NVM_DIR/nvm.sh" ]] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[[ -s "$NVM_DIR/bash_completion" ]] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

setopt histignorespace
