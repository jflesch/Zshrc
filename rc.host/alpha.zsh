# Prompts color

# Default prompt is:
# <date> (user~host) path

export date_color="white"
export user_color="blue"
export host_color="red"
export path_color="gray"
export status_color="red"
