# Prompts color

# Default prompt is:
# <date> (user~host) path
 
export date_color="white"
export user_color="green"
export host_color="green"
export path_color="green"
export status_color="red"
