# Prompts color

# Default prompt is:
# <date> (user~host) path

export date_color="white"
export user_color="red"
export host_color="green"
export path_color="yellow"
export status_color="red"
