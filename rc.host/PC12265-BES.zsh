export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
# export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_131

# workaround crappy gvim on host system
if [ ! -e /tmp/jflesch_sid_chroot ]; then
	schroot -b -c sid > /tmp/jflesch_sid_chroot
fi
export SID_CHROOT=$(< /tmp/jflesch_sid_chroot)
alias schroot='autoschroot'

alias gvim='autoschroot gvim'
alias lnav='autoschroot lnav'
alias gtags='autoschroot gtags'
alias global='autoschroot global'

# make chromium happy
export auto_proxy=http://proxy.parkeon.com/proxy.pac

function autoschroot {
    export GDK_BACKEND=x11
    export GDK_RENDERING=image
    if [ -z "${SCHROOT_CHROOT_NAME}" ]; then
        \schroot -c ${SID_CHROOT} -r -- "$@"
    else
        "$@"
    fi
    return $?
}

function b0rk.py {
    if ! which adb > /dev/null 2>&1 ; then
        source ~/bin/buildenv
    fi

    local ret

    #	adb connect 10.32.51.136
    ~/work/git/tools/buildHelpers/builders/buildChildren.py "$@"
    return $?
}

alias logcat='adb logcat -v threadtime'

function tlogcat {
    if ! which adb > /dev/null 2>&1 ; then
        source ~/bin/buildenv
    fi
    logcat_file=logcat-$(date "+%Y%m%d_%H%M%S").txt
    echo "=== Logcat file output stored in: ~/tmp/${logcat_file} ==="
    logcat | tee ~/tmp/${logcat_file} | lnav
    echo "=== Logcat file output stored in: ~/tmp/${logcat_file} ==="
}
