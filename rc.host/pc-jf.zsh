# Prompts color

# Default prompt is:
# <date> (user~host) path
 
export date_color="white"
export user_color="red"
export host_color="red"
export path_color="red"
export status_color="green"
export mail_color="yellow"

umask 022
