# Prompts color

# Default prompt is:
# <date> (user~host) path

export date_color="white"
export user_color="green"
export host_color="red"
export path_color="blue"
export status_color="red"
