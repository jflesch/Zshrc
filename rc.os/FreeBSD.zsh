# Alias

alias date-rfc822="date '+%a, %d %b %Y %X %z'"

source $HOME/.zsh/rc.os/prompt.zsh
source $HOME/.zsh/rc.os/common.zsh

# Check for GNULS
if [ -x "$(which gnuls)" ] ; then
	eval `dircolors $HOME/.zsh/misc/dircolors.rc`
	alias ls='gnuls --color'
	zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
fi

bindkey "\e[1~" beginning-of-line
bindkey "\e[7~" beginning-of-line
bindkey "\e[2~" overwrite-mode
bindkey "\e[3~" delete-char
bindkey "\e[4~" end-of-line
bindkey "\e[8~" end-of-line

setopt prompt_subst
RPS1='%{$fg[$status_color]%}$(__prompt_git)%{$reset_color%}'

if [ -z "${XDG_CONFIG_HOME}" ]
then export XDG_CONFIG_HOME=${HOME}/.config
fi
