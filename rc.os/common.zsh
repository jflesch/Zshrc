function __prompt_git()
{
    if ! [ -x "$(which git 2>/dev/null)" ]
    then exit 0
    fi

    local git_dir ref br top;
    git_dir=$(git rev-parse --git-dir 2> /dev/null) || return
    ref=$(git symbolic-ref HEAD 2> /dev/null) || return
    br=${ref#refs/heads/}
    top=$(cat $git_dir/patches/$br/current 2>/dev/null) \
        && top="/$top"
    echo "[$br$top]"
}

# function use to toggle RPS1 (which is very ugly for copy/paste)
function rmrps1 () {
	OLDRPS="${RPS1}"
	unset RPS1
}

function rps1 () {
    setopt prompt_subst
	RPS1="${OLDRPS}"
}

