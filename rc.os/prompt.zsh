# Prompt
autoload -U colors
colors

# Format
date_format="%H:%M"

chroot=""
if [ -n "$SCHROOT_CHROOT_NAME" ]; then
	chroot="[%{$fg[yellow]%}${SCHROOT_CHROOT_NAME}%{$reset_color%}]"
fi

date="%{$fg[$date_color]%}%D{$date_format}"

host="%{$fg[$user_color]%}%n%{$reset_color%}~%{$fg[$host_color]%}%m%{$reset_color%}${chroot}"

cpath="%{$fg[$path_color]%}%/%b"
end="%{$reset_color%}"

PS1="$date$end ($host$end) $cpath
$end%% "
