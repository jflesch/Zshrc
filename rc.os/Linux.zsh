alias ls='ls --color'
alias grep='grep --color'

if [ -d ${HOME}/git/liquidprompt ]; then
	source ${HOME}/git/liquidprompt/liquidprompt
else
	source $HOME/.zsh/rc.os/prompt.zsh
	source $HOME/.zsh/rc.os/common.zsh

	# permit parameter expansion, command substitution and arithmetic expansion
	# in prompt
	setopt prompt_subst

	cpath="%B%{$fg[$path_color]%}\$PWD%b"
	end="%{$reset_color%}"
	PS1="$date$end ($host$end) $cpath
$end%% "

	setopt prompt_subst
	RPS1='%{$fg[$status_color]%}$(__prompt_git)%{$reset_color%}'

	export PS1 RPS1
fi

# zstyle OS specific
eval `dircolors $HOME/.zsh/misc/dircolors.rc`

# Use LS_COLORS for color completion
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}


bindkey '\e[1~'   beginning-of-line  # Linux console
bindkey '\e[H'    beginning-of-line  # xterm
bindkey '\eOH'    beginning-of-line  # gnome-terminal
bindkey '\e[2~'   overwrite-mode     # Linux console, xterm, gnome-terminal
bindkey '\e[3~'   delete-char        # Linux console, xterm, gnome-terminal
bindkey '\e[4~'   end-of-line        # Linux console
bindkey '\e[F'    end-of-line        # xterm
bindkey '\eOF'    end-of-line        # gnome-terminal
bindkey '^[[Z'    reverse-menu-complete  # shift-tab
